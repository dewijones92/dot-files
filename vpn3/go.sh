#!/usr/bin/env bash
set -x;
set +e;
set +o pipefail

currentPID="$BASHPID"

if [ "$EUID" -ne 0 ]
  then echo "Please run as root"
  exit
fi

target_dir="/tmp/vpnstuff"
rm -rf $target_dir
mkdir -p $target_dir

vpn_file_path="$target_dir/conf.conf"

newPath="/home/pi/tmp/vpn_files/index/OpenVPN 2023/OpenVPN/BE_Brussels_TCP.ovpn"
oldPath="/home/pi/OpenVPN 2023/OpenVPN 2023/OpenVPN/BE_Brussels_OB_UDP.ovpn"
cp "$newPath" $vpn_file_path
(cd $HOME/Downloads/OpenVPN-Configs && cp Wdc.key ca.crt $target_dir)

cp $HOME/code/dewi_projects/ivacy_vpn_auth $target_dir/passfile


vpn_docker_client="openvpn-client"

docker rm -f $vpn_docker_client

docker run  -d \
   --restart=always \
  --cap-add=NET_ADMIN \
  --name=$vpn_docker_client \
  -e VPN_AUTH_SECRET="/data/vpn/passfile" \
  --device /dev/net/tun \
  --volume $target_dir:/data/vpn \
  dewivpn 

clientpid=$(docker inspect -f '{{.State.Pid}}'  $vpn_docker_client)
echo $clientpid

netns_name="vpn_client"

pids_nets=$(ip netns pids $netns_name)
echo $pids_nets
echo $pids_nets | xargs -r -t kill -9

ip netns delete $netns_name
mkdir -p /var/run/netns
touch /var/run/netns/$netns_name
mount -o bind /proc/$clientpid/ns/net /var/run/netns/$netns_name


mkdir -p /etc/netns/$netns_name/
echo '' > /etc/netns/$netns_name/resolv.conf
echo 'nameserver 8.8.8.8' > /etc/netns/$netns_name/resolv.conf



ssh_server_name="ssh_server_name"
docker rm -f $ssh_server_name

qbitorrent_name="qbitorrent_name"
docker rm -f $qbitorrent_name

docker run -d  --restart=always --name=$qbitorrent_name -it --network=container:$vpn_docker_client  -v $HOME/code/dot-files/serverconfig/root_mnt_folders/home/dewi/.config/qBittorrent:/config -v $HOME/torrentDownloads:/downloads lscr.io/linuxserver/qbittorrent 


docker run -d \
  --restart=always \
  --name=$ssh_server_name \
  -e TZ=Europe/London \
  -e PASSWORD_ACCESS=true `#optional` \
  -e USER_PASSWORD=lol `#optional` \
  -e USER_NAME=dewi `#optional` \
  -e DOCKER_MODS='linuxserver/mods:openssh-server-ssh-tunnel' `#optional` \
  --network=container:$vpn_docker_client \
  -v ssh_vpn_conf:/config \
  lscr.io/linuxserver/openssh-server:latest


ip netns ls
ip netns exec $netns_name bash
