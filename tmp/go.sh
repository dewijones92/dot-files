#!/bin/bash

# Function to measure curl response time for a given IP
measure_response_time() {
  local ip=$1
  local start_time=$(date +%s.%N)
  # Timeout after 5 seconds
  if ! curl -s -o /dev/null -w "%{time_total}" -m 5 "http://${ip}/"; then
    echo "Error or timeout with IP: $ip" >&2
    return 1
  fi
  local end_time=$(date +%s.%N)
  local duration=$(echo "$end_time - $start_time" | bc)
  echo "$duration $ip"
}

# Export the function
export -f measure_response_time

# Read and validate IP addresses from stdin
ip_addresses=$(grep -o -E '\b([0-9]{1,3}\.){3}[0-9]{1,3}\b' source | awk '
  BEGIN { valid = 1 }
  {
    split($0, a, ".")
    if (a[1] > 223 || a[1] == 0 || a[4] == 0 || a[4] == 255) {
      print "Invalid IP: " $0 > "/dev/stderr"
      valid = 0
    } else {
      print $0
    }
  }
  END { exit !valid }
')

# Check if any valid IPs were found
if [[ -z "$ip_addresses" ]]; then
  echo "No valid IP addresses found." >&2
  exit 1
fi

# Parallel execution using xargs and finding the first successful response
fastest_ip=$(
  {
    echo "$ip_addresses" | xargs -P $(nproc) -I {} bash -c "measure_response_time {}" |
      while read -r duration ip; do
        echo "$ip"
        # Kill all xargs processes after the first successful response
        pkill -P $$ xargs
      done
  } | head -n 1
)

# Output the fastest IP
echo "Fastest IP: $fastest_ip"
