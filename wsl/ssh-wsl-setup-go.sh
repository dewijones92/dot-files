#!/bin/bash

# README This script creates and manages a custom SSH daemon service for WSL (Windows Subsystem for Linux). It allows you to specify a custom port, allowed users, and ensures the service starts automatically on boot.

# Define variables
SERVICE_FILE="/etc/systemd/system/custom-wsl-sshd.service"  # Changed service file name
SSHD_PATH="/usr/sbin/sshd"
PORT=2223
USER_ALLOWED="dewi"

# Create the service file
sudo bash -c "cat > $SERVICE_FILE" <<EOL
[Unit]
Description=Custom WSL SSH Daemon  # Changed description
After=network.target

[Service]
ExecStart=$SSHD_PATH -D -p $PORT \
          -o PasswordAuthentication=yes \
          -o PermitRootLogin=yes \
          -o AllowUsers=$USER_ALLOWED \
          -e
Restart=always
RestartSec=5
User=root

[Install]
WantedBy=multi-user.target
EOL

# Reload systemd daemon to recognize the new service
sudo systemctl daemon-reload

# Enable the service to start on boot
sudo systemctl enable custom-wsl-sshd.service  # Changed service name

# Start the service immediately
sudo systemctl start custom-wsl-sshd.service  # Changed service name

# Display the status of the service
sudo systemctl status custom-wsl-sshd.service  # Changed service name
