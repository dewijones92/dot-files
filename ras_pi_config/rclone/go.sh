set -x
set -e
systemctl disable rclone-files.service
systemctl disable rclone-files.timer
ln -sf $(pwd)/rclone-files.service /etc/systemd/system
ln -sf $(pwd)/rclone-files.timer /etc/systemd/system
systemctl enable rclone-files.service
systemctl enable rclone-files.timer
systemctl start rclone-files.timer
