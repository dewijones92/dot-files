# Raspberry Pi Rclone Backup System
SEARCH BITWARDEN FOR WASABI

This system automatically syncs files from your Raspberry Pi to Wasabi S3 storage using rclone with encryption.

## Prerequisites

- Raspberry Pi with Docker installed
- Wasabi S3 account with access and secret keys
- Bitwarden (or similar) for secure storage of credentials

## Initial Setup

### 1. Directory Structure

Create the required directories:

```bash
mkdir -p ~/.xdg_config/rclone
mkdir -p ~/ext_files/public
```

### 2. Configure Rclone

1. Create `~/.xdg_config/rclone/rclone.conf`:

```bash
# First, ensure directory exists
mkdir -p ~/.xdg_config/rclone

# Create and edit the config file
nano ~/.xdg_config/rclone/rclone.conf
```

2. Add the following configuration (replace placeholders with your Wasabi credentials):

```ini
[dewi3]
type = s3
provider = Wasabi
access_key_id = YOUR_WASABI_ACCESS_KEY
secret_access_key = YOUR_WASABI_SECRET_KEY
endpoint = s3.eu-central-1.wasabisys.com
acl = private

[dewi_secret]
type = crypt
remote = dewi3:dewi3
password = YOUR_ENCRYPTION_PASSWORD
```

### 3. Deploy Services

Clone the repository containing the service files and run the setup script:

```bash
cd ~/code/dot-files/ras_pi_config/rclone
./go.sh
```

## System Components

### Service Configuration

The system consists of three main components:

1. **Timer**: Runs daily at 3 AM
2. **Service**: Executes the rclone sync operation
3. **Docker Container**: Runs rclone in an isolated environment

### Security Notes

- Credentials are stored in two locations:
  1. Wasabi access/secret keys in rclone.conf
  2. Encryption password in rclone.conf
- Backup these credentials securely in Bitwarden with the following structure:
  ```json
  // Entry: "wasabisys"
  {
    "access_key": "your_access_key",
    "secret_key": "your_secret_key"
  }
  
  // Entry: "s3 wasabi secret"
  "your_encryption_password"
  ```

## Troubleshooting

### Common Issues

1. If the service fails to start, check:
   ```bash
   systemctl status rclone-files.service
   journalctl -u rclone-files.service
   ```

2. To test rclone configuration:
   ```bash
   docker run --rm --volume ~/.xdg_config/rclone:/config/rclone \
              --volume ~/ext_files/public:/data:shared \
              rclone/rclone:latest listremotes
   ```

### Manual Sync

To manually trigger a sync:

```bash
systemctl start rclone-files.service
```

## Recovery Process

If you need to set this up on a new Raspberry Pi:

1. Install Docker
2. Create required directories
3. Retrieve credentials from Bitwarden
4. Set up rclone.conf
5. Deploy services using go.sh

## Data Location

- Local files: `~/ext_files/public/`
- Remote destination: `dewi_secret:dewi3`
- Configuration: `~/.xdg_config/rclone/rclone.conf`
