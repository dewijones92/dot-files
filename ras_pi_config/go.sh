set -x
sudo systemctl daemon-reload
systemctl disable sox_brown.service
ln -s $(pwd)/sox_brown.service  /etc/systemd/system
systemctl enable sox_brown.service
systemctl start sox_brown.service
systemctl restart sox_brown.service

(cd ssh_connect && bash generate_and_install.sh)


#cd ..
#cd syslogs
#systemctl stop sys_logs.service
#systemctl disable sys_logs.service
#ln -fs $(pwd)/sys_logs.service /etc/systemd/system
#systemctl enable sys_logs.service
#systemctl start sys_logs.service
#systemctl restart sys_logs.service
#sudo systemctl daemon-reload
