set -x

base_path="/home/pi/code/dot-files/bash_stuff/helpers"
exec unbuffer $base_path/bparallel_commands "$base_path/journlogs" "$base_path/dmesglogs"  "ping -vvv 1.1.1.1" "ping -vvv 192.168.1.1" "$base_path/netcatlogs" | ts >> /home/pi/syslog.log
