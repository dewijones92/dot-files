
set -x

function generate_ssh_files {
	mkdir -p bin/$1
	cp  secure-tunnel-vultr.service.template  bin/$1/secure-tunnel-$1.service
	cp  go.sh.template  bin/$1/go.sh
	cd bin/$1
	\ls | \xargs -t sed -i "s/{{PORT}}/$1/g" 

	vultr_port=$1
	if test $1 -eq 22
	then 
		vultr_port=23
	fi

	\ls | \xargs -t sed -i "s/{{VULTR_PORT}}/$vultr_port/g" 
}
	




rm -rf bin/
base_path="$(pwd)"
ports=( 22 )
for i in "${ports[@]}";
do
	echo "----------------------"
	echo "generating for port $i"
	generate_ssh_files "$i"

	systemctl disable secure-tunnel-$i.service
	ln -fs $(pwd)/secure-tunnel-$i.service /etc/systemd/system
	systemctl enable secure-tunnel-$i.service
	systemctl start secure-tunnel-$i.service
	systemctl restart secure-tunnel-$i.service
	cd $base_path
done

