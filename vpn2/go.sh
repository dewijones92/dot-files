set -x;
netns1="dewinetns_1"
netns2="dewinetns_2"

qbittorrent_client="qbittorrent_client"
qbittorrent_sever="qbittorrent_server"

openvpn_client="openvpn_client"
openvpn_server="openvpn_server"

docker_run="docker run --user 1000";

$docker_run -d --rm --name $qbittorrent_client -v "$(pwd)/$qbittorrent_client/config":/config \
	-v "$(pwd)/$qbittorrent_client/downloads":/downloads \
		lscr.io/linuxserver/qbittorrent;

function clean {
	docker rm -f $qbittorrent_client;
}

clean
