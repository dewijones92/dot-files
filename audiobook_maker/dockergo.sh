set -x
set -e
mkdir -p "${HOME}/.local/share/mycroft/mimic3"
chmod a+rwx "${HOME}/.local/share/mycroft/mimic3"
# curl -X POST --data 'Hello world.' --output - localhost:59125/api/tts
# exit
docker run -d \
	--name dewi-tts \
       -i \
       -p 59125:59125 \
       -v "${HOME}/.local/share/mycroft/mimic3:/home/mimic3/.local/share/mycroft/mimic3" \
       'mycroftai/mimic3'  

