set -e
set -x

function cleanup() {
	set -x
	kill_all_child_pids_recursive $$
}

trap cleanup EXIT

bookpath="$1"

rm -rf srcTextParts
mkdir srcTextParts
#removed duplicate lines except for blank lines. then splits the book up by line
cat "$bookpath" | awk '!NF || !seen[$0]++' | awk -v RS="" '{print $0 > "srcTextParts/" (i++ + 1)".txt"; close("srcTextParts/"i".txt")}'

#exit
#rm -rf ./audioOut
mkdir audioOut

stt_on_each_text_part () {
        set -x
        set -e
        textPartPath="$1"
        cat "$textPartPath" | stt | \grep -i -a -v "error" \
                 | ffmpeg -i pipe:0 -vn -ar 44100 -ac 1 -b:a 92k -f mp3 pipe:1  \
                > audioOut/$(basename ${textPartPath}).audio.mp3
                 #|   sox -t raw -r 44100 -e signed -b 16 -c 2 - -t raw - \

}

export -f stt_on_each_text_part

\find srcTextParts/   | sort -n -t / -k 2  \
        | xargs -t -n1 -t -I{} bash -ci 'stt_on_each_text_part "$@"' _ {}
	| ffmpeg -i pipe:0 -vn -ar 44100 -ac 1 -b:a 92k -f mp3 pipe:1


exit;
#to join them together in zsh
-----
zsh:

audio-join() {
  local files=("${(f)$(printf '%s\n' $@)}")
  ffmpeg -i "concat:$(printf '%s|' "${(@)files[2,-1]}")" -acodec copy "$1"
}

audio-join ../output.mp3 *.mp3(n)
------

