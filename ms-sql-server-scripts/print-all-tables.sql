SET NOCOUNT ON;

DECLARE @tableName NVARCHAR(128);
DECLARE @cmd NVARCHAR(MAX);

-- Cursor to loop through all table names in the current database
DECLARE table_cursor CURSOR FOR
SELECT name FROM sys.tables;

OPEN table_cursor;
FETCH NEXT FROM table_cursor INTO @tableName;

WHILE @@FETCH_STATUS = 0
BEGIN
    BEGIN TRY
        -- Print the table header
        PRINT '################ Table: ' + @tableName;

        -- Build the dynamic SQL command to select all records from the table
        SET @cmd = N'SELECT * FROM ' + QUOTENAME(@tableName) + ';';

        -- Execute the dynamic SQL command
        EXEC sp_executesql @cmd;

        -- Print an empty line for readability
        PRINT '';
    END TRY
    BEGIN CATCH
        -- Capture and print the error message without terminating the script
        PRINT 'Error with table ' + @tableName + ': ' + ERROR_MESSAGE();
    END CATCH

    -- Fetch the next table name
    FETCH NEXT FROM table_cursor INTO @tableName;
END

-- Clean up the cursor
CLOSE table_cursor;
DEALLOCATE table_cursor;
