set -x
set -e
export minVideoLengthMins=20
export outdir="/tmpfs/yt-dl-podcasts/"
mkdir -p outdir || true

function ydownload {
 set -euxo pipefail
 durationSeconds=$((minVideoLengthMins * 60))
 yt-dlp -x --dateafter now-2days -f 'ba' --no-overwrites -o "$outdir/%(upload_date>%Y-%m-%d)s-%(title)s-%(id)s.%(ext)s"  "$1"\
	 |& tee /dev/stderr /dev/stdout | \grep -m1 "upload date is not in range"
}
export -f ydownload

set +e
cat channellist.txt | xargs -P0 -I{} -t -n1 -d"\n" bash -xc 'ydownload "$@"' _ {}

 rsync -e 'ssh -p 8022' --progress $outdir/* 192.168.0.171:storage/music/NewPipe
