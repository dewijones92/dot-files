set -x;
set -e;

if [ "$EUID" -ne 0 ]
  then echo "Please run as root"
  exit
fi

source ./common.sh

cleanup
cleanPids;

sig_handler() {
    echo "CLEANING"
    echo "sig_handler"
    exit_status=$?
    echo "Doing signal-specific up"
    cleanup;
    cleanPids;
}

trapFunc "sig_handler"

ip netns add $NETNAME;

ip link add veth0 type veth peer name veth1;
ip link set veth1 netns $NETNAME;
#exit;

ip addr add 10.1.3.1/24 dev veth0;
ip netns exec $NETNAME ip addr add $NETNS_IP dev veth1;
ip link set dev veth0 up
ip netns exec $NETNAME ip link set dev veth1 up

ip netns exec $NETNAME ip link set lo up

ip netns exec $NETNAME ip link list;

ip netns exec $NETNAME ip addr;


iptables -A FORWARD -o $default_interface -i veth0 -j ACCEPT
iptables -A FORWARD -i $default_interface -o veth0 -j ACCEPT
iptables -t nat -A POSTROUTING -s $NETNS_IP -o $default_interface -j MASQUERADE
ip netns exec $NETNAME ip route add default via 10.1.3.1

mkdir -p /etc/netns/$NETNAME/
echo '' > /etc/netns/$NETNAME/resolv.conf
echo 'nameserver 8.8.8.8' > /etc/netns/$NETNAME/resolv.conf


function runCmd {
	(ip netns exec $NETNAME $1 & savepid) |& sed "s/^/[$2] /"  & savepid
}
export -f runCmd



#runCmd "dnsmasq  -d" "dns"

#runCmd "nc -vvv -k -l 5555" "nc"
runCmd "nc -vvv -k -l 7777" "nc7777"

set +e
userdel -r vpnuser
groupadd -r openvpn
groups
useradd vpnuser
usermod -a -G openvpn vpnuser
passwd --delete  vpnuser
#usermod -G root vpnuser
set -e


ip netns exec $NETNAME bash -i vpn_iptable2.sh &
#runCmd "openvpn   --config   /home/dewi/code/dot-files/vpn/Ukraine-TCP.ovpn  --auth-user-pass /home/dewi/code/dewi_projects/ivacy_vpn_auth" "openvpn"

iptables -A INPUT -i veth0 -j ACCEPT
iptables -A OUTPUT -o veth0 -j ACCEPT
iptables -A FORWARD -o veth0 -j ACCEPT

#ip netns exec $NETNAME qbittorrent-nox
