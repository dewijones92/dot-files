set -x
default_interface=$(route | \grep default | tr -s " " | cut -d " " -f8);
echo $default_interface;

export NETNAME="dewinetns235";
NETNS_IP="10.1.3.2/24"
export PIDPATHS="/tmp/netvpids$NETNAME";

getPidPathFile () {
	set -x
	iam="$(whoami)"
	dpath="${PIDPATHS}_${iam}"
	echo "$dpath"
}
export -f getPidPathFile

function savepid {
	set -x;
	pid=$!
	pname="$(ps -p $pid -o comm=)"
	id;
	echo "save name";
	saveFilePath="$(getPidPathFile)"
	echo $saveFilePath
	savetext="$pid $pname";
	echo $savetext >> "$saveFilePath";
}
export -f savepid


function cleanup {
    	echo "CLEANUP"
	set -x;
	set +e;
	ip netns pids $NETNAME | xargs -t kill -9
	ip link delete veth0;
	ip link delete veth1;
	ip netns delete $NETNAME;
}

function cleanPids {
	set +e
	writePath="$(getPidPathFile)"
	cat $writePath
	cat $writePath | cut -d " " -f1 | xargs -t -n1 -d"\n" kill -9
	> "$writePath";
}

function trapFunc {
	trap $1 HUP INT QUIT PIPE TERM SIGQUIT SIGINT SIGTSTP
}

set -e;
