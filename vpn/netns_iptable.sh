set -x;
#remove/flush all rules & delete chains
iptables -F

iptables -F
iptables -P INPUT   ACCEPT
iptables -P OUTPUT   ACCEPT
iptables -P FORWARD   ACCEPT

ippaddr="188.72.0.0/16"
iptables -A INPUT -s $ippaddr  -j ACCEPT
iptables -A OUTPUT -d $ippaddr  -j ACCEPT

iptables -A INPUT -s $ippaddr  -j ACCEPT
iptables -A OUTPUT -d $ippaddr  -j ACCEPT

iptables -A INPUT -p icmp -j ACCEPT
iptables -A OUTPUT -p icmp -j ACCEPT
iptables -A FORWARD -p icmp -j ACCEPT


