set -x
echo "gotorrent"
cd $1
pwd
source ./common.sh

cleanPids;

trapFunc "cleanPids"

function runCmd {
	($1 & savepid) |& sed "s/^/[$2] /"  & savepid
}
export -f runCmd

runCmd "qbittorrent-nox" "qtorrent"
runCmd "nc -vvv -k -l 5556" "sctest"
runCmd "ping 1.1.1.1" "dewiping"
