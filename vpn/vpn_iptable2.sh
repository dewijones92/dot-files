# Flush the tables. This may cut the system's internet.
set -x
pwd
#
ipc="iptables -w 20"
$ipc -F

# Let the VPN client communicate with the outside world.
$ipc -A OUTPUT -j ACCEPT -m owner --uid-owner root
$ipc -A INPUT  -s 10.1.3.0/24 -j ACCEPT
$ipc -A OUTPUT  -d 10.1.3.0/24 -j ACCEPT

# The loopback device is harmless, and TUN is required for the VPN.
$ipc -A INPUT -j ACCEPT -i lo
$ipc -A OUTPUT -j ACCEPT -o lo
$ipc -A FORWARD -j ACCEPT -o lo

$ipc -A OUTPUT -j ACCEPT -o tun+
$ipc -A INPUT -j ACCEPT -i tun+
$ipc -A FORWARD -j ACCEPT -o tun+
    
# We should permit replies to traffic we've sent out.
$ipc -A INPUT -j ACCEPT -m state --state ESTABLISHED

# The default policy, if no other rules match, is to refuse traffic.
$ipc -P OUTPUT DROP
$ipc -P INPUT DROP
$ipc -P FORWARD DROP
runuser -l  dewi -c "bash -i $(pwd)/torrentGo.sh $(pwd)"  |& sed "s/^/[dewitorrent] /"
