set -x
set -e

read -p "Do you want to delete all of Chings data???" answer

if [ "$answer" = "y" ]; then
    echo "Continuing..."
    # Do something here
else
    echo "Exiting."
    exit 0
fi

if [ "$EUID" -ne 0 ]
  then echo "Please run as root"
  exit
fi

ching_guac="ching_guac"
ching_rdp="ching_rdp"


#	  -v /ching_rdp_file_system_home:/home \

docker rm -f $ching_rdp; 
 docker run \
	 --security-opt seccomp=unconfined \
	 --shm-size="1gb"  -d --name=$ching_rdp      -e PUID=1000   -e PGID=1000   -e TZ=Etc/UTC   -v /var/run/docker.sock:/var/run/docker.sock `#optional`   -v /path/to/data:/config `#optional`   --device /dev/dri:/dev/dri `#optional` -p 8080:8080 -p 3390:3389  --restart unless-stopped   lscr.io/linuxserver/rdesktop:ubuntu-xfce


clientpid=$(docker inspect -f '{{.State.Pid}}'  $ching_rdp)
echo $clientpid

netns_name="ching_netns"

set +e
pids_nets=$(ip netns pids $netns_name)
echo $pids_nets
echo $pids_nets | xargs -r -t kill -9

ip netns delete $netns_name
mkdir -p /var/run/netns
touch /var/run/netns/$netns_name
mount -o bind /proc/$clientpid/ns/net /var/run/netns/$netns_name
set -e


mkdir -p /etc/netns/$netns_name/
echo '' > /etc/netns/$netns_name/resolv.conf
echo 'nameserver 8.8.8.8' > /etc/netns/$netns_name/resolv.conf




docker rm -f $ching_guac || true
docker run  --shm-size="1gb" --restart unless-stopped -d --name="$ching_guac" --network=container:"$ching_rdp" --device /dev/dri:/dev/dri  -v DEWI_GUACAMOLE:/config    maxwaldorf/guacamole

