#!/bin/bash
set -x
set -e

if [ "$EUID" -ne 0 ]; then
  echo "Please run as root"
  exit 1
fi

sudo nft flush ruleset
nft -f /home/pi/code/dot-files/bash_stuff/ras_pi_firewall/iptables_data_nf_tables

echo "Firewall rules applied."
