#!/bin/bash
set -x
set -e

if [ "$EUID" -ne 0 ]; then
  echo "Please run as root"
  exit 1
fi

# Ensure /etc/docker/daemon.json exists and has the correct content
mkdir -p /etc/docker
echo '{
  "ip": "127.0.0.1"
}' > /etc/docker/daemon.json

# Install and enable the firewall service
systemctl disable raspberrypi_firewall.service || true # Disable if it exists
ln -sf "$(realpath "$(dirname "$0")")"/raspberrypi_firewall.service /etc/systemd/system/

systemctl daemon-reload

systemctl enable raspberrypi_firewall.service
systemctl start raspberrypi_firewall.service

echo "Firewall installed and started."
