#!/bin/bash
set -x
set -e
export BW_SESSION="$(bw unlock | \grep "BW_SESSION" | head -n2 | tail -n1 | cut -d"\"" -f2)"

getBwAttachment () {
        ID="$1"
        FILE="$2"
        ID="$1" && FILE="$2" && bw get attachment --quiet $(bw get item $ID | jq ".attachments[] | select((.fileName == \"$FILE\")).id" -r) --output /dev/stdout --itemid $ID 2>/dev/null  2>/dev/null
}

#EXAMPLE
#fileBrowserJsonSecret=$(getBwAttachment "72d5d26a-b4ff-4b52-b6f1-af8b00a06cbd" "filebrowser.json")
getBwAttachment "$1" "$2"
