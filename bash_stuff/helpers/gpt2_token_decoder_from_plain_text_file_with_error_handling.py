#!/usr/bin/env python3

import argparse
import numpy as np
import tiktoken
import os

def validate_file(file_path):
    if not os.path.exists(file_path):
        raise argparse.ArgumentTypeError(f"The file {file_path} does not exist")
    return file_path



def decode_numbers(file_path):
    # Initialize the GPT-2 tokenizer
    enc = tiktoken.get_encoding("gpt2")

    with open(file_path, 'r') as file:
        content = file.read()
    
    numbers = content.split()
    
    for number in numbers:
        try:
            num = int(number)
            decoded = enc.decode([num])
            print(f"{num:6d} = {repr(decoded)}")
        except ValueError:
            print(f"{number:6s} = Invalid number")
        except Exception as e:
            print(f"{num:6d} = Error: {str(e)}")

def main():
    parser = argparse.ArgumentParser(description="Decode numbers using GPT-2 tokenizer")
    parser.add_argument("file_path", type=validate_file, help="Path to the file containing numbers")
    args = parser.parse_args()

    decode_numbers(args.file_path)

if __name__ == "__main__":
    main()
