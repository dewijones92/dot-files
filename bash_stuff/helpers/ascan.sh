set -x

function cleanup()
{
    set -x
    kill_all_child_pids_recursive $$
}
trap cleanup EXIT


ascan () {
 set -x
 ub arp-scan --interface="$1" --localnet |& ub sed "s/^/ascan-if-[$1]/" 
}
export -f ascan;

nmap_func_on_interface () {
	cat <(echo "david22") <(ub nmap -F -sS "$1/24") |& ub sed "s/^/nmap[$1]/" 
}
export -f nmap_func_on_interface

nmap_each_interface () {
	ub ip addr show | ub grep inet | ub grep -v inet6 \
		| ub awk '{print $NF": "$2}' | ub sed 's/\/.*//' |  ub cut -d":" -f 2 \
		| ub sed 's/^[[:space:]]*//;s/[[:space:]]*$//' \
		| xargs -I{} -P0 -t  \
		bash -cx 'nmap_func_on_interface "$@"' _ {}
}
export -f nmap_each_interface




ub list_interfaces | ub xargs -P0 -t -n1 -d"\n" -I{} bash -c 'ascan "$@"' _ {} &
ub bash -xc nmap_each_interface &
tail -f /dev/null
exit


