#!/usr/bin/python3
import json
import argparse

def generate_json_from_file(filename):
    # Define the base structure
    base_structure = {
        "name": "",
        "listen": "[::]:{}",
        "upstream": "host.docker.internal:{}",
        "enabled": True
    }

    output = []

    # Read the port numbers from the file
    with open(filename, 'r') as file:
        ports = file.readlines()

    # Iterate over the port numbers and generate the JSON structure
    for idx, port in enumerate(ports):
        port = port.strip()  # Remove any whitespace or newline
        entry = base_structure.copy()
        entry["name"] = f"rbq_{idx + 1}"
        entry["listen"] = entry["listen"].format(port)
        entry["upstream"] = entry["upstream"].format(port)
        output.append(entry)

    return json.dumps(output, indent=2)

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Generate JSON structure from a file containing port numbers.")
    parser.add_argument('filepath', type=str, help="Path to the file containing port numbers.")
    
    args = parser.parse_args()
    
    print(generate_json_from_file(args.filepath))

