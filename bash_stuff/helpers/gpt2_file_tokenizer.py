#!/usr/bin/env python3
import os
import argparse
import tiktoken
import sys

def tokenize_file(file_path):
    # Initialize the GPT-2 tokenizer
    enc = tiktoken.get_encoding("gpt2")
    
    # Read the file content
    with open(file_path, 'r', encoding='utf-8') as file:
        content = file.read()
    
    # Tokenize the content
    tokens = enc.encode(content)
    
    return tokens

def validate_file_path(file_path):
    if not os.path.exists(file_path):
        raise argparse.ArgumentTypeError(f"The file '{file_path}' does not exist.")
    if not os.path.isfile(file_path):
        raise argparse.ArgumentTypeError(f"'{file_path}' is not a file.")
    if not os.access(file_path, os.R_OK):
        raise argparse.ArgumentTypeError(f"The file '{file_path}' is not readable.")
    return file_path

def main():
    parser = argparse.ArgumentParser(description="Tokenize a file using GPT-2 tokenizer")
    parser.add_argument("file_path", type=validate_file_path, help="Path to the file to be tokenized")
    
    try:
        args = parser.parse_args()
    except argparse.ArgumentTypeError as e:
        print(f"Error: {str(e)}")
        sys.exit(1)

    try:
        tokens = tokenize_file(args.file_path)
        print(f"Number of tokens: {len(tokens)}")
        print("All tokens:")
        print(tokens)
    except Exception as e:
        print(f"An error occurred while tokenizing the file: {str(e)}")
        sys.exit(1)

if __name__ == "__main__":
    main()
