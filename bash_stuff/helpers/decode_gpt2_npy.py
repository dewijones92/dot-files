#!/usr/bin/env python3

import argparse
import numpy as np
import tiktoken
import os

def decode_file(file_path):
    # Load the numpy array
    tokens = np.load(file_path)
    
    # Initialize the GPT-2 tokenizer
    enc = tiktoken.get_encoding("gpt2")
    
    # Decode the tokens
    text = enc.decode(tokens.tolist())
    
    # Decode individual tokens
    individual_decodes = []
    for token in tokens:
        decoded = enc.decode([token])
        individual_decodes.append(f"{token} = {decoded!r}")
    
    return text, individual_decodes

def main():
    # Set up argument parser
    parser = argparse.ArgumentParser(description="Decode a .npy file containing GPT-2 tokens.")
    parser.add_argument("file_path", type=str, help="Path to the .npy file to decode")
    
    # Parse arguments
    args = parser.parse_args()
    
    # Validate file path
    if not os.path.exists(args.file_path):
        print(f"Error: File '{args.file_path}' does not exist.")
        return
    
    if not args.file_path.endswith('.npy'):
        print(f"Error: File '{args.file_path}' is not a .npy file.")
        return
    
    # Decode and print the text
    try:
        decoded_text, individual_decodes = decode_file(args.file_path)
        print("Decoded text:")
        print(decoded_text)
        print("\nIndividual token decodes:")
        for decode in individual_decodes:
            print(decode)
    except Exception as e:
        print(f"An error occurred while decoding the file: {str(e)}")

if __name__ == "__main__":
    main()
