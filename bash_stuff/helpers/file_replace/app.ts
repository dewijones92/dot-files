#!/usr/bin/env node
import * as fs from 'fs';

if (process.argv.length !== 5) {
  console.error('Usage: node findreplace file1 file2 "{{PLACEHOLDER}}"');
  process.exit(1);
}

const file1 = process.argv[2];
const file2 = process.argv[3];
const placeholder = process.argv[4];

const file1Contents = fs.readFileSync(file1, 'utf-8').trim();
const file2Contents = fs.readFileSync(file2, 'utf-8').trim();

const replacedContents = file1Contents.replace(new RegExp(placeholder, 'g'), file2Contents);

fs.writeFileSync(file1, replacedContents);
console.log(`Replaced ${placeholder} in ${file1} with contents of ${file2}`);

