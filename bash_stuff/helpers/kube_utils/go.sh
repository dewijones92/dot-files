#!/usr/bin/env bash

set -x;
set +e;

rm -rf bin/
mkdir bin
cp kub_detailsTEMPLATE bin/kube_details

filereplace \
	bin/kube_details \
	content \
	"{{COMMON}}"


chmod +x bin/*
