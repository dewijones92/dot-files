#!/bin/bash
set -x
set -e


sortUnique () {
  cat - | sort -r -u -k2 -s
}

#make sure only one instance of this script is running
flock_lock_location="/var/tmp/bash_hist_crypt_backup.lock"
exec 100>$flock_lock_location || exit 1
flock -n 100 || exit 1
trap "rm -f $flock_lock_location" EXIT

# Get the current user ID
user_id=$(id -u)
# Check if the user ID is 1000
if [ "$user_id" -ne 1000 ]; then
  echo "not user 1000 - Sorry, can't do it."
  exit
fi

repo_root="$HOME/code/bash_hist"


cd "$HOME/code/bash_hist"

gf
origin_master_commit_hash=$(git ls-remote origin -h refs/heads/master)
current_commit_hash=$(git rev-parse HEAD)

newBashHistFromOtherPCs="0"

if [ "$origin_master_commit_hash" = "$current_commit_hash" ]; then
    echo "no new bash history from other PCs"
else
    echo "NEW BASH HIST FROM OTHER PCS. GONNA MERGE LATER"
    newBashHistFromOtherPCs=1
fi


gpg --list-keys
git-crypt unlock;
git-crypt status;
folder="bash_history/pc-$COMPUTER_NAME"
mkdir -p $folder
(cd "bash_history"; \ls "pc*"  | xargs -t -n1 | sortUnique > /tmpfs/.bash_history_merged)

#merge histories - make unique
mergedHist=$(cat "$folder/bash_history" ~/.bash_history | sortUnique);
echo $mergedHist > "$folder/bash_history"; 



git add .

#Check if all the files have been encrypted
git crypt status -e | \grep -i -c warning;
if [ $? -eq 0 ]; then exit; fi
set -e

git commit -m "add history"
git-crypt lock;

gf;
git rebase origin/master;
git push
rm /tmp/tmpfile
