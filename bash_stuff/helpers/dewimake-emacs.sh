# run this in the root of the cloned emacs repo
set -x
./autogen.sh autoconf
./configure --prefix=$(pwd)/dewi --with-rsvg;
make install
sudo ln -s $(pwd)/dewi/bin/emacs /usr/bin/emacs
sudo ln -s $(pwd)/dewi/bin/emacsclient /usr/bin/emacsclient
