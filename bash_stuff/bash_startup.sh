
FFFFFFFFF="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )";


set +e
source /usr/share/doc/fzf/examples/key-bindings.bash 
source /usr/share/doc/fzf/examples/completion.bash 

export PATH="$HOME/.ghcup/bin":$PATH

dewi_path="$HOME/.linkerd2/bin:/$HOME/istio-1.17.2/bin:$HOME/.bin:$HOME/code/ktlint/ktlint/build/run:$HOME/code/kotlin-language-server/server/build/install/server/bin:$HOME/.cabal/bin/:$DEWI_SETTING_BASE/bash_stuff/helpers/overrides:$DEWI_SETTING_BASE/bash_stuff/helpers:$DEWI_SETTING_BASE/bash_stuff/helpers/utils_to_backup:$DEWI_SETTING_BASE/bash_stuff/helpers/gitlab_runner_dir:$DEWI_SETTING_BASE/bash_stuff/helpers/kube_utils/bin:$DEWI_SETTING_BASE/bash_stuff/helpers/file_replace/bin:$DEWI_SETTING_BASE/bash_stuff/helpers/kube_utils/bin:$HOME/.dotnet/tools:$HOME/code/.fzf/bin:$HOME/code/miscellaneous_scripts"

export REPO_BASE=$(cd $DEWI_SETTING_BASE && cd .. && pwd);

export PATH=~/.npm-global/bin:$PATH
export PATH=$dewi_path:/home/dewi/.local/bin:/$REPO_BASE/doom-emacs/bin:$PATH:$dewi_path;
export PATH=$PATH:/usr/local/go/bin
export PATH=$PATH:$HOME/go/bin
export PATH=$PATH:$HOME/code/linux-timemachine
source "$REPO_BASE/dewi_projects/startup.sh"
export DOT_BASE="$REPO_BASE/dot-files"




#alias stuff
alias ls="ls -althN --color=auto -r"
alias watch="watch -n0.5 -d"


set -o vi

PATH="/home/dewi/code/tmux":$PATH
PATH="/home/dewi/Android/Sdk/tools":$PATH
PATH="/home/dewi/Android/Sdk/platform-tools":$PATH


export ANDROID_HOME=$HOME/Android/Sdk
export ANDROID_SDK_ROOT=$HOME/Android/Sdk
export PATH=$PATH:$ANDROID_HOME/tools
export PATH=$PATH:$ANDROID_HOME/tools/bin
export PATH=$PATH:$ANDROID_HOME/platform-tools
export PATH=$PATH:$ANDROID_HOME/build-tools/30.0.3
export ANDROID_NDK_HOME="$ANDROID_HOME/ndk"


export PATH=$PATH:/home/dewi/code/care/miscellaneous/useful_scripts

source "$HOME/.cargo/env"

export PATH=$HOME/code/emacs/dewi/bin:$PATH
export PATH=$HOME/code/socat:$PATH

#alias python="/usr/bin/python3"
#alias pip="/usr/bin/pip3"
. "$HOME/.cargo/env"



export DEFAULT_EDITOR="nvim";
export VISUAL=$DEFAULT_EDITOR;
export EDITOR="$VISUAL";
alias v=$DEFAULT_EDITOR;
alias t="type -a";
alias d="colordiff";
eval "$(hub alias -s)";

ot () {
	\xargs -P0 -n1 -L1 \grep -lI -E "*"
}
export -f ot

print_something () {
	echo asdasd
}



gt () {
	git for-each-ref --sort=taggerdate --format '%(refname) %(taggerdate)' refs/tags | less
}
export -f gt

alias bl="brew ls -ltv";

export MANPAGER="$EDITOR -c 'set ft=man' -";
alias c="clear && printf '\e[3J'"




LS_COLORS='rs=0:di=1;35:ln=01;36:mh=00:pi=40;33:so=01;35:do=01;35:bd=40;33;01:cd=40;33;01:or=40;31;01:su=37;41:sg=30;43:ca=30;41:tw=30;42:ow=34;42:st=37;44:ex=01;32:*.tar=01;31:*.tgz=01;31:*.arj=01;31:*.taz=01;31:*.lzh=01;31:*.lzma=01;31:*.tlz=01;31:*.txz=01;31:*.zip=01;31:*.z=01;31:*.Z=01;31:*.dz=01;31:*.gz=01;31:*.lz=01;31:*.xz=01;31:*.bz2=01;31:*.bz=01;31:*.tbz=01;31:*.tbz2=01;31:*.tz=01;31:*.deb=01;31:*.rpm=01;31:*.jar=01;31:*.war=01;31:*.ear=01;31:*.sar=01;31:*.rar=01;31:*.ace=01;31:*.zoo=01;31:*.cpio=01;31:*.7z=01;31:*.rz=01;31:*.jpg=01;35:*.jpeg=01;35:*.gif=01;35:*.bmp=01;35:*.pbm=01;35:*.pgm=01;35:*.ppm=01;35:*.tga=01;35:*.xbm=01;35:*.xpm=01;35:*.tif=01;35:*.tiff=01;35:*.png=01;35:*.svg=01;35:*.svgz=01;35:*.mng=01;35:*.pcx=01;35:*.mov=01;35:*.mpg=01;35:*.mpeg=01;35:*.m2v=01;35:*.mkv=01;35:*.webm=01;35:*.ogm=01;35:*.mp4=01;35:*.m4v=01;35:*.mp4v=01;35:*.vob=01;35:*.qt=01;35:*.nuv=01;35:*.wmv=01;35:*.asf=01;35:*.rm=01;35:*.rmvb=01;35:*.flc=01;35:*.avi=01;35:*.fli=01;35:*.flv=01;35:*.gl=01;35:*.dl=01;35:*.xcf=01;35:*.xwd=01;35:*.yuv=01;35:*.cgm=01;35:*.emf=01;35:*.axv=01;35:*.anx=01;35:*.ogv=01;35:*.ogx=01;35:*.aac=00;36:*.au=00;36:*.flac=00;36:*.mid=00;36:*.midi=00;36:*.mka=00;36:*.mp3=00;36:*.mpc=00;36:*.ogg=00;36:*.ra=00;36:*.wav=00;36:*.axa=00;36:*.oga=00;36:*.spx=00;36:*.xspf=00;36:';
export LS_COLORS

#PS1='\e[37;1m\u@\e[35m\W\e[0m\$ ' # this will change your prompt format

alias te="~/Dropbox/Coding/alacritty/target/release/alacritty </dev/null &>/dev/null &"
alias gb="git branch --list -a -v"

alias gbv="git for-each-ref --color=always --sort=-committerdate refs/heads/ refs/remotes/ --format='%(HEAD) %(color:yellow)%(refname:short)%(color:reset) - %(color:red)%(objectname:short)%(color:reset) - %(contents:subject) - %(authorname) (%(color:green)%(committerdate:relative)%(color:reset)) - %(committerdate)' | less -r -I -"

alias db="dotnet build -c Release"
alias dr="dotnet run --no-build -c Release"
alias dw="cd $DEWI_BASE";

gp () {
	git push --all origin
}

gbs () {
	git checkout $1 2>/dev/null || git checkout -b $1
}
export -f gbs

h () {
	htop -d 5
}
export -f h


alias lynx="lynx -cfg=~/.lynxrc"
alias n="nnn"



alias pt="pstree -aAlhpsStTU"
alias l="less -Ri"
alias net="sudo netstat -vla --numeric-ports"




export PAGER="less -i"


tmux_to_vim () {
	tmux capture-pane -pS -32768 | awk '{$1=$1;print}' | nvim +$
}
export -f tmux_to_vim

pkg_list () {
	expac --timefmt='%Y-%m-%d %T' '%l\t%n'|sort -n

}
export -f pkg_list

s () {
	ss -npae
}
export -f s

p () {
	ps -aux --forest
}
export -f p

hw_o () {
	lspci -vvvkknn;
	hwinfo;
	lsusb -v;
	lscpu;
	lsmod;
  set -x;
}
export -f hw_o;
hw () { sudo hw_o; } 
export -f hw



sstest_o () {
	touch /tmp/lololol;
	touch /tmp/lololol2;
}

dew_test () { sudo sstest_o; }
export -f dew_test



extract () {
	[[ -z "$1" ]] && { echo "Parameter 1 is empty" ; return 1; }
	ext_dir=$(echo $1 | cut -d'.'  -f1-1)
	mkdir "$ext_dir"
	cd "$ext_dir"
	7z x ../"$1"
	cd ..
}
export -f extract

nmgui () {
	nm-applet    2>&1 > /dev/null &
	stalonetray  2>&1 > /dev/null
	killall nm-applet
}

export -f nmgui;

getPrograms () {
	compgen -A function -abck;
}

export -f getPrograms;
f () {
	program=$(getPrograms | fzf)
	history -s $program
	echo $program
}
export -f getPrograms;

choose_program () {
  getPrograms | sort -u | fzf | xargs c_exec
}
export -f choose_program;

launcher () {
 #alacritty --class 'launcher' --command bash -c 'getPrograms | sort -u | fzf | xargs c_exec'
 alacritty --class 'launcher' --command bash -c 'choose_program'
}

export -f launcher;

gfs () {
        (git ls-files && git ls-files . --exclude-standard --others) | sort -u;
}
export -f gfs;


selectByFzf () {
	sourceList=$1
	local selected=$(printf '%q' "$($sourceList | fzf)")
	READLINE_LINE="${READLINE_LINE:0:$READLINE_POINT}$selected${READLINE_LINE:$READLINE_POINT}"
	READLINE_POINT=$(( READLINE_POINT + ${#selected} ))
}

readMergedBashHist () {
	tac "/tmpfs/.bash_history_merged"
}



export -f selectByFzf;

bind -x '"\C-P":selectByFzf gfs'

bind -x '"\C-L":selectByFzf readMergedBashHist'




export FZF_DEFAULT_OPTS='--no-height --no-reverse'
export FZF_CTRL_T_OPTS="--preview '(highlight -O ansi -l {} 2> /dev/null || cat {} || tree -C {}) 2> /dev/null | head -200'"

tompv () {
	files=$(find  | \grep -E "recover.*.js.*$")
	cat $files | sed "s/\\_closedTabs.*//" | sed "s/{/\n{/g"
 }
export -f tompv;

escape_to_regex (){
	printf '%s' "$1" | sed 's/[.[\*^$]/\\&/g'
}



screengrab () {
	grim -g "$(slurp)" - | wl-copy
}
export -f screengrab;

kp () {
	tokill=$(p | \grep "$1" | \grep -v "grep" | tr -s " ");
	read -p "Wanna kill\n \"$tokill?\"" -n 1 -r
	echo    # (optional) move to a new line
	if [[ $REPLY =~ ^[Yy]$ ]]
	then
		# do dangerous stuff
		echo "$tokill" | cut -d' ' -f2  - | xargs  -t -n1 -L1   -I {} kill -9 "{}"
	fi
}
export XDG_CONFIG_HOME="$HOME/.xdg_config"



NPM_PACKAGES="${HOME}/.npm-packages"

export PATH="$PATH:$NPM_PACKAGES/bin"

# Preserve MANPATH if you already defined it somewhere in your config.
# Otherwise, fall back to `manpath` so we can inherit from `/etc/manpath`.
export MANPATH="${MANPATH-$(manpath)}:$NPM_PACKAGES/share/man"

# Avoid duplicates
export HISTCONTROL=ignoredups:erasedups
# When the shell exits, append to the history file instead of overwriting it
export HISTCONTROL=ignoredups:erasedups  # no duplicate entries
export HISTSIZE=-1
export HISTFILESIZE=-1
shopt -s histappend

# After each command, append to the history file and reread it
export PROMPT_COMMAND="history -a; history -c; history -r"

export MAKEFLAGS="-j$(expr $(nproc) \+ 1)"
eval "$(stack --bash-completion-script stack)"

source /home/dewi/code/fzf/shell/*.bash
source /home/dewi/code/fzf/shell/key-bindings.bash

vcat () {
	echo "---------"
	echo "$1"
	cat "$1"
}
export -f vcat

gcat () {
	cat "$1" | awk '{printf "%d\t%s\n", NR, $0}' | sed "s@^@[$1]@" 
}
export -f gcat

escapeString() { printf '%q\n' "$1";  }; export -f escapeString;

vvcat () {
	cat | xargs -P0 -d"\n" -n1 -I{} bash -c 'escapeString "$@"' _ {} | ot 2>/dev/null |xargs -P0 -d"\n" -I{} -n1  bash -c 'gcat "$@"' _ {} |  \grep  -I  -i --colour --line-buffered -i -E "$@" 2>/dev/null
}

fss () {
	find | vvcat $@
}

gfss () {
	gfs | vvcat $@
}
export -f gfss


remove_hist_envs () {
	declare -a files=("bashrc" "profile" "bash_profile")
	declare -a keywords=("HISTCONTROL" "HISTSIZE" "HISTFILESIZE")
	for i in "${files[@]}"
	do
		for j in "${keywords[@]}"
		do
		echo "$i $j"
		sed -i "/$j/d" "$HOME/.$i"
		done
	done
}
remove_hist_envs &>/dev/null


backup_hist () {
	filename=".bash_history";
	backupFile="$HOME/${filename}_BACKUP" 
	tmpFile="/tmp/$filename"
	cat "$backupFile" "$HOME/$filename" | awk '!x[$0]++'  > "$tmpFile"
	cat "$tmpFile" > "$backupFile"
	rm "$tmpFile"
}
backup_hist

dclean () {
	set -x
	rm -rf .git/ ./git-crypt/ ./bash_history/
	git init
	git add "README.md"
	git commit -m "init"

	git-crypt init
	gpg --list-keys
	git-crypt add-gpg-user  C0B0BFE7E42E21D12C2C258CC48810D1C5EA36BD
	git status

	git add .
	git commit -m "add history"
	git-crypt lock

	git remote add origin git@gitlab.com:dewijones92/bash_hist.git
	git push --set-upstream origin master
	git push
}

kill_all_child_pids_recursive () {
	set -x
	unset HISTFILE
	rm /tmp/ff
	log="tee -a /tmp/ff"
	parentpid="$1"
	currentpid="$$"
	echo "start DEWI DEWI $(($(date +%s%N)/1000000))" >> /tmp/ff
	cpids=$(pstree -p $parentpid | ub tee /dev/stderr |  $log | \grep -v $1 | $log  | grep -Eo '\([0-9]+\)' | grep -Eo '[0-9]+' | $log | base64 -w 0)
	echo "end $(($(date +%s%N)/1000000))" >> /tmp/ff
	echo $cpids;
	echo "dewi"
	bash -xc "echo $cpids | base64 -d | xargs -t kill -9" &
}

#this unlocks the gpg keyring so the gpg now works in a script
bash "$HOME/code/bash_hist/run.sh" |& sed 's/^/HIST_BACKUP:/' &> /dev/null &

getIpOfVpnContainer () {
	vpn_con_ip=$(docker inspect openvpn-client  | \grep -E ".*\"IPAddress\"" | \head -n1 | \cut -d "\"" -f4);
	echo $vpn_con_ip
}
export -f getIpOfVpnContainer
unset cdiff;

export DOTNET_ROOT=$HOME/.dotnet
export PATH=$PATH:$DOTNET_ROOT:$DOTNET_ROOT/tools


try_ssh_big_server () {
	set -x
	 while ! ssh -t dewi@192.168.0.143 'set -x; tmux attach || tmux && bash -i'; do sleep 0.2; done ;
}

emacs_big_server () {
	set -x
	while ! ssh -t dewi@192.168.0.143 bash -ci 'start_emacs'; do sleep 0.2; done ;
}


export DBUS_SESSION_BUS_ADDRESS="unix:path=/run/user/1000/bus"
export XDG_RUNTIME_DIR="/run/user/1000"
e() {
  emacsclient -c -s instance1 "$@" 
}

sd () {
 set -x
 current_dir=$(pwd);
 sudo -i bash -x -i <<<  "source /home/dewi/.profile ;cd $current_dir; exec </dev/tty" ;
}




