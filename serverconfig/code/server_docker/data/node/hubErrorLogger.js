var bodyParser = require('body-parser')//add this
const fs = require('fs');

const express = require('express')
const app = express()
const port = 3000


app.use(bodyParser())//add this before any route or before using req.body

app.listen(port, () => {
  console.log(`Example app listening at http://localhost:${port}`)
})

app.use((req, res, next) => {
  console.log(req.body); // this is what you want           
  const logged = `\n\n------------------------------------------------------------------\ncurrent utc time: ${new Date()}  - ${JSON.stringify(req.body)}`

  fs.appendFile('/dewilogs/huberror.log', logged , function (err) {
    if (err) throw err;
    console.log('Saved!');
   });

  res.on("finish", () => {

    console.log(res);

  });

  next();

});


app.post('/', (req, res) => {
  res.send('thanks for the error')
})
