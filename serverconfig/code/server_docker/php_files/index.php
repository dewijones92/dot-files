<?php
error_reporting(E_ALL);
ini_set('display_errors', '1');

error_reporting(-1);
ini_set('display_errors', 'On');


function console_log( $data ){
  return;
  echo '<script>';
  echo 'console.log('. json_encode( $data ) .')';
  echo '</script>';
}

header('Content-type: text/xml');
/*
	Runs from a directory containing files to provide an
	RSS 2.0 feed that contains the list and modification times for all the
	files. Thanks to vsoch on GitHub!

*/
$feedName = "Dewi server";
$feedDesc = "Audiobooks etc";
$feedURL = "https://{{TK_DOMAIN}}/rss/{{SECRET_URL}}";
$feedBaseURL = $feedURL . "/files/"; // must end in trailing forward slash (/).

$allowed_ext = ".mp4,.MP4,.mp3,.MP3,.m4b,m4a";

?><<?= '?'; ?>xml version="1.0"<?= '?'; ?>>
<rss version="2.0" xmlns:atom="http://www.w3.org/2005/Atom"  xmlns:itunes="http://www.itunes.com/dtds/podcast-1.0.dtd">
	<channel>
		<title><?=$feedName?></title>
		<link><?=$feedURL?></link>
		<description><?=$feedDesc?></description>
                <itunes:image href="https://upload.wikimedia.org/wikipedia/commons/a/a9/George_Monbiot_%28from_his_website%2C_cropped%29.jpg"/>
<?php

$dir = "/files/*";
$items = array();
foreach(glob($dir) as $file)
{
        if(is_dir($file)) { continue; }
        echo basename($file)."\n";
        console_log( " filesize " );
        console_log(  filesize($file));
        $item['name'] = basename($file);
        $item['timestamp'] = filemtime($file);
        $item['size'] = filesize($file);
        $items[] = $item;
}

console_log($items);

foreach($items as $item) {
	if($item['name'] != "index.php") {
          if (!empty($item['name'])) {
		$fileLink = $feedBaseURL . $item['name'];
		echo "	<item>\n";
		echo "		<title>". $item['name'] ."</title>\n";
		echo "		<link>". $fileLink . "</link>\n";
		echo "		<guid>". $fileLink . "</guid>\n";
		echo "		<enclosure url=\"$fileLink\"/>\n";
		echo "		<pubDate>". date(DATE_RSS, $item['timestamp']) ."</pubDate>\n";

		echo "    </item>\n";
	  }
	}
}
?>
	</channel>
</rss>
