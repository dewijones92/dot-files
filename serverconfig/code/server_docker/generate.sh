#!/bin/bash
if [[ $(/usr/bin/id -u) -ne 0 ]]; then
    echo "Not running as root"
    exit
fi

set -x

echo "dewi!!!!!!!!"

for ((i=0; i<=$#; i++)); do
  printf "%s: %s\n" "$i" "${!i}"
done

echo "end dewi!!"

# Exporting input arguments as environment variables
export tk_domain="$1"
export duckdns_domain="$2"
export ext_files="$3"
export run_user="$4"
export secret_url="$5"
export duckdns_token="$6"
export ching_laptop_ethernet_mac_address="$7"
export cloudflare_api_token="$8"

export secure_dir="/private/secure/dir"
mkdir -p "$secure_dir"
sudo chmod 700 "$secure_dir"


# Create the external files directory
mkdir -p "$ext_files"

# Create and set the base directory with restricted permissions
bin="bin"
# Remove existing bin directory if necessary
rm -rf "$bin"
mkdir -m 700 "$bin"  # Set permissions to rwx for owner only
# Change ownership to root
sudo chown root:root "$bin"
export base_dir="$(pwd)/$bin"

# Replace `gfs` with `git ls-files` to list only tracked files
git ls-files | xargs -n1 -d"\n" -t -I{} cp --parents -r "{}" "$bin"

# Function to perform string replacement using sed
sed_func () {
    local filepath="$1"
    local src_replace="$2"
    local dst_replace="$3"
    sed -i "s&$src_replace&$dst_replace&g" "$filepath"
}
export -f sed_func

# Function to replace all placeholders in a file
replace () {
    set -x
    local filepath="$1"
    sed_func "$filepath" '{{TK_DOMAIN}}' "$tk_domain"
    sed_func "$filepath" '{{DUCKDNS_TOKEN}}' "$duckdns_token"
    sed_func "$filepath" '{{DUCKDNS_DOMAIN}}' "$duckdns_domain"
    sed_func "$filepath" '{{EXT_FILES}}' "$ext_files"
    sed_func "$filepath" '{{BASE_DIR}}' "$base_dir"
    sed_func "$filepath" '{{HOME_DIR}}' "$HOME"
    sed_func "$filepath" '{{RUN_USER}}' "$run_user"
    sed_func "$filepath" '{{SECRET_URL}}' "$secret_url"
    sed_func "$filepath" '{{CHING_LAPTOP_ETHERNET_MAC}}' "$ching_laptop_ethernet_mac_address"
    sed_func "$filepath" '{{CLOUDFLARE_API_TOKEN}}' "$cloudflare_api_token"
    sed_func "$filepath" '{{SECURE_DIR}}' "$secure_dir"
}
export -f replace

# Apply the replace function to all relevant files in the bin directory
(
    cd "$bin" || exit
    find . -type f ! -name "README" | xargs -d"\n" -I{} bash -c 'replace "$0"' "{}"
)

# Secure the bin directory and its contents
# Change ownership to root and set permissions to 700 recursively
sudo chown -R root:root "$bin"
sudo chmod -R 700 "$bin"

echo "Configuration complete. The bin directory is now secured."
