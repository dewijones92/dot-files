set -x;
sudo sh -c "truncate -s 0 /var/lib/docker/containers/*/*-json.log"
dewi_docker_log="dewi_docker_log"
curl_name="dewi_curl"
echo "$dewi_docker_log 0"


( docker-compose pull  && docker-compose up -d --remove-orphans);
docker-compose up -d;

#docker-compose up --force-recreate --build -d



# Define the network name
network="private"

# Check if the Docker network exists
if ! docker network ls --filter name=^${network}$ --format "{{.Name}}" | grep -w ${network} > /dev/null; then
    echo "Docker network '${network}' does not exist. Creating it..."
    docker network create --driver bridge ${network}
    if [ $? -ne 0 ]; then
        echo "Failed to create Docker network '${network}'. Exiting."
        exit 1
    fi
else
    echo "Docker network '${network}' already exists."
fi

# Update the docker_run variable to use the 'private' network instead of 'host'
docker_run="docker run -d --restart unless-stopped --network ${network}"


# Remove the existing curl container if it exists
docker rm -f "$curl_name"
echo "${dewi_docker_log} 1"

# (Optional) Uncomment and configure the following lines if you need to run the curl container
# docker_run --name "$curl_name" curlimages/curl watch -n60 curl -vvv https://nouser:IGNORE@www.duckdns.org/v3/update?hostname=333133333 &

echo "${dewi_docker_log} 2"

# Define the PHP container name
php_name="dewi_php"

# Remove the existing PHP container if it exists
docker rm -f "${php_name}"
echo "${dewi_docker_log} 3"

# Run the PHP container with the specified volumes and working directory
${docker_run} \
    --name "${php_name}" \
    -v /home/pi/ext_files/media/audiobooks/:/files:ro \
    -v /home/pi/code/dot-files/serverconfig/code/server_docker/bin/php_files/index.php:/usr/src/myapp/index.php \
    -w /usr/src/myapp \
    php:7.4-cli php -S localhost:8919 &


# Remove the existing Wake-on-LAN container if it exists
docker rm -f ching-laptop-wake-on-lan

# Run the Wake-on-LAN container within the 'private' network
${docker_run} \
    --name ching-laptop-wake-on-lan \
    ching-laptop-wake-on-lan

echo "${dewi_docker_log} 4"
