set -x;
set -e;
echo "DEWI START";

echo $PATH;

cd /home/dewi/code/dot-files/serverconfig;
ls;
(cd root_mnt_files/ && CURRENTDIR=$(pwd) && find | tail -n +2 | xargs -n1 -d"\n" -t  -I{} ln -sf $CURRENTDIR/{} /{} )

set +e;
ip addr add 192.168.1.1 dev enp7s1
ip link set enp7s1 up
set -e;

ls;
echo "dewihmmmmmm"
#source ./iptable_stuff.sh


#systemctl  disable isc-dhcp-server
cd root_mnt_folders;
#ln -sf $(pwd)/etc/dhcp /etc/dhcp
mount --bind $(pwd)/etc/dhcp /etc/dhcp
mount --bind $(pwd)/etc/systemd/network /etc/systemd/network
mount --bind $(pwd)/home/dewi/.config/qBittorrent /home/dewi/.config/qBittorrent
iptables-restore  < /etc/iptables/rules.v4
 
iptables -A INPUT -p icmp -j ACCEPT
iptables -A OUTPUT -p icmp -j ACCEPT


#systemctl daemon-reload;
#systemctl enable dewistart.service

systemctl  start isc-dhcp-server

systemctl restart systemd-networkd.service
