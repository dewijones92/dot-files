clone to $HOME/code 
git@github.com:emacs-mirror/emacs.git

run this: bash_stuff/helpers/dewimake.sh
- this symlinks to /usr/bin/emamcs
- this symlinks to /usr/bin/emamcsclient

# systemd service - run as NON ROOT
ln -s $(pwd)/emacs/emacs.service ~/.config/systemd/user/emacs.service
systemctl start emacs.service
systemctl enable  emacs.service

