# Start of the script
Write-Host "Welcome to the server utility script"

# Prompt the user
$WakeServer = Read-Host -Prompt "Do you want to wake the server? Please answer Yes or No"

# Check the user's response
If ($WakeServer -eq "Yes") {  
    # Open Notepad if they said "Yes"
    Start-Process Notepad.exe
}

# Kill all Alacritty windows
Get-Process | Where-Object {$_.ProcessName -eq "alacritty"} | Stop-Process -Force

alacritty.exe -e bash -c "ls; ssh  pi@dewijones92vultr.duckdns.org  bash -ic w_server"
alacritty.exe -e bash -c "ls; ssh -t pi@dewijones92vultr.duckdns.org 'tmux attach || tmux && bash -i'"


alacritty.exe -e bash -c "ls; ssh -t pi@dewijones92vultr.duckdns.org 'bash -ic try_ssh_big_server'"
alacritty.exe -e bash -c "ls; ssh -t pi@dewijones92vultr.duckdns.org 'bash -ic emacs_big_server'"


