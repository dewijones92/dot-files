from flask import Flask, request
import subprocess

app = Flask(__name__)

@app.route('/wakeup/<mac_address>', methods=['GET'])
def wakeup(mac_address):
    try:
        # Execute the wakeonlan command
        subprocess.run(['wakeonlan', mac_address], check=True)
        return f"Sent WOL packet to {mac_address}", 200
    except subprocess.CalledProcessError as e:
        return str(e), 500

if __name__ == '__main__':
    print("hello")
    app.run(host='0.0.0.0', port=5091)
