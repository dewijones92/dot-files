set -x
TARGET_DIR=$(pwd)/bin/
TARGET_FILE=out.vim
TARGET=$TARGET_DIR$TARGET_FILE
mkdir -pv $TARGET_DIR;
cat sensible.vim   dewi.vim  dim/default-light.vim > $TARGET 

LN_TARGET="$XDG_CONFIG_HOME/nvim/"
mkdir -p $LN_TARGET
ln -sf "$TARGET" "$LN_TARGET/init.vim"
